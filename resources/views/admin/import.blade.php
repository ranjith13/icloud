@extends('admin.layouts.app')
@section('title', 'Import')
@section('subtitle', 'Files')
@section('content')
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <!-- card -->
                    @if(Session::has('message'))
{{ Session::get('message') }}
@endif
                    <div class="card scrollmenu">
                        <!-- card-header -->
                        <div class="card-header">
                            <h3 class="card-title">Import File</h3>
                        </div>
                        <div class="loader" style="display:none;"></div>
                        <div class="data-section">
                          <!-- /.card-header -->
                          <!-- card-body -->
                          <form id="offer_code_import" action="{{ route('file-import') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="card-body">
                              <div class="form-group">
                                <label>Please upload files only in the formats : CSV</label>
                                <input type="file" name="upload_file" id="upload_file" class="form-control" placeholder="Choose CSV File" accept=".csv" autocomplete="off">
                             
                              </div>
                              <div style="margin-top: 2%;margin-left: 5px;">Please keep the upload size less then : 10MB</div>
                            </div>
                            <div class="card-footer">
                              <a href="" download="purple_members_sample_file.csv" class="btn btn-secondary btn-sm">Download Sample</a>
                              <!-- <button type="button" class="btn btn-outline-secondary btn-sm" id="" >Download Sample</button> -->
                              <button type="submit" class="btn btn-primary" id="offer_code_import_form_submit" style="float: right;">Upload</button>
                            </div>
                          </form>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
  <!-- /.content-wrapper -->

@endsection

@section('script')

<script>
  $(function() {
    $(".loader").hide();
    $(".data-section").show();
    $('#offer_code_import').validate({
        rules: {
            upload_file: {
                required: true,
            }
        },
        messages: {
            upload_file: {
                required: "Please select your CSV file",
            }
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
        });
    });

    $("#offer_code_import_form_submit").click(function(e) {
        $(".data-section").hide();
        $(".loader").show();
    });
</script>

@endsection
