<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\User;

use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::updateOrCreate([
            'email'     => 'admin@gmail.com',
        ],[
            'name'                  => 'Admin',
            'password'              => Hash::make('Admin@123'),
         /*    'role'                  => 'admin' */
        ]);
    }
}
