@extends('admin.layouts.app')
@section('title', 'Branches')
@section('subtitle', 'Branches')
@section('content')
<div class="card-body">
<table class="table table-bordered table-striped table-branches" style="width:100%">
<thead>
<tr>
<th>S. No.</th>
<th>Branches</th>                                     
</tr>
</thead>
</table>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
  $(function () {
    $(".loader").hide();
    $(".data-section").show();
    var table = $('.table-branches').DataTable({
        bStateSave: true,
        responsive: true,
        bDestroy: true,
        processing: true,
        serverSide: true,
        deferRender: true,
        "pageLength": 10,
        "scrollX": true,
        dom: 'Bfrtip',
        lengthMenu: [
            [ 10, 25, 50, 500 ],
            [ '10 rows', '25 rows', '50 rows', '500 rows' ]
        ],
        buttons: [
            'pageLength','colvis','copy', 'csv', 'excel', 'print'
        ],
        ajax: "{{ route('branch') }}",
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
            {data: 'Branch_name', name: 'Branch_name'}
             ]
    });
  });
</script>
@endsection

