<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFinancialTransdetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('financial_transdetails', function (Blueprint $table) {
            $table->id();
            $table->integer('financialTranId');
            $table->integer('moduleId');
            $table->integer('amount');
            $table->integer('headId');
            $table->integer('crdr');
            $table->integer('brid');
            $table->integer('head_name');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('financial_transdetails');
    }
}
