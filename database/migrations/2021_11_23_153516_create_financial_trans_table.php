<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFinancialTransTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('financial_trans', function (Blueprint $table) {
            $table->id();
            $table->integer('moduleid');
            $table->integer('tranid');
            $table->integer('amount');
            $table->text('crdr');
            $table->text('tranDate');
            $table->string('acadYear');
            $table->string('entrymode',100);
            $table->integer('Voucherno');
            $table->integer('brid');
            $table->integer('dueAmount')->nullable();
            $table->integer('consession/scholarship')->nullable();
            $table->integer('duerev')->nullable();
            $table->integer('consession/scholarshipRev')->nullable();
            

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('financial_trans');
    }
}
