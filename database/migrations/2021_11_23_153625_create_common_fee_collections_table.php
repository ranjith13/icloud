<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommonFeeCollectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('common_fee_collections', function (Blueprint $table) {
            $table->id();
            $table->integer('moduleId');
            $table->integer('transId');
            $table->string('admno');
            $table->string('rollno');
            $table->integer('amount');
            $table->integer('brId');
            $table->string('academicYear',100);
            $table->string('financialYear',100);
            $table->text('displayReceiptNo');
            $table->integer('Entrymode');
            $table->text('Paid_Date');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('common_fee_collections');
    }
}
