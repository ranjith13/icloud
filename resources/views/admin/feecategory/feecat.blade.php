@extends('admin.layouts.app')
@section('title', 'category')
@section('subtitle', 'category')
@section('content')
<div class="card-body">
<table class="table table-bordered table-striped table-category" style="width:100%">
<thead>
<tr>
<th>S. No.</th>
<th>Branch</th>                                     
<th>Fee Category</th>                                     
</tr>
</thead>
</table>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
  $(function () {
    $(".loader").hide();
    $(".data-section").show();
    var table = $('.table-category').DataTable({
        bStateSave: true,
        responsive: true,
        bDestroy: true,
        processing: true,
        serverSide: true,
        deferRender: true,
        "pageLength": 10,
        "scrollX": true,
        dom: 'Bfrtip',
        lengthMenu: [
            [ 10, 25, 50, 500 ],
            [ '10 rows', '25 rows', '50 rows', '500 rows' ]
        ],
        buttons: [
            'pageLength','colvis','copy', 'csv', 'excel', 'print'
        ],
        ajax: "{{ route('fee-category') }}",
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
            {data: 'br_id', name: 'br_id'},
            {data: 'fee_category', name: 'fee_category'}
             ]
    });
  });
</script>
@endsection

