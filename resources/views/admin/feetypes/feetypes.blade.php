@extends('admin.layouts.app')
@section('title', 'Fee Types')
@section('subtitle', 'Fee Types')
@section('content')
<div class="card-body">
<table class="table table-bordered table-striped table-types" style="width:100%">
<thead>
<tr>
<th>S. No.</th>
<th>fee_category</th>                                     
<th>f_name</th>                                     
<th>Collection_id</th>                                     
<th>br_id</th>                                     
<th>Seq_id</th>                                     
<th>Fee_type_ledger</th>                                     
<th>Fee_headtype</th>                                                                        
</tr>
</thead>
</table>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
  $(function () {
    $(".loader").hide();
    $(".data-section").show();
    var table = $('.table-types').DataTable({
        bStateSave: true,
        responsive: true,
        bDestroy: true,
        processing: true,
        serverSide: true,
        deferRender: true,
        "pageLength": 10,
        "scrollX": true,
        dom: 'Bfrtip',
        lengthMenu: [
            [ 10, 25, 50, 500 ],
            [ '10 rows', '25 rows', '50 rows', '500 rows' ]
        ],
        buttons: [
            'pageLength','colvis','copy', 'csv', 'excel', 'print'
        ],
        ajax: "{{ route('fee-types') }}",
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
            {data: 'br_id', name: 'br_id'},
            {data: 'fee_category', name: 'fee_category'},
            {data: 'f_name', name: 'f_name'},
            {data: 'Collection_id', name: 'Collection_id'},
            {data: 'br_id', name: 'br_id'},
            {data: 'Seq_id', name: 'Seq_id'},
            {data: 'Fee_type_ledger', name: 'Fee_type_ledger'},
            {data: 'Fee_headtype', name: 'Fee_headtype'}
             ]
    });
  });
</script>
@endsection

