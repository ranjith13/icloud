<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Announcement;
use App\Http\Requests;
use App\Models\branches;
use App\Models\feecategory;
use App\Models\feecollectiontype;
use App\Models\Fee_types;
use App\Models\Financial_trans;
use App\Models\Common_fee_collection;
use App\Models\common_fee_collection_headwise;
use App\Models\financial_transdetails;
class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
/*   public function __construct()
    {
        $this->middleware('auth');
    }  */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('admin.home');
    }

    public function masters()
    {
        return view('admin.masters');
    }
    public function importview() {

        return view('admin.import');

    }
    public function import(Request $request) {

        $return_message = '';
        $rules = $this->ImportCsvRules();
        $messages = [
            'upload_file.uploaded' => "The file size should be less than 10MB",
            'upload_file.max' => 'The file size should be less than 10MB',
            'upload_file.mimes' => 'The file should be in CSV format'
        ];
        $this->validate($request, $rules,$messages);
        $upload_file = $request->upload_file;
        //Clear cache and check filesize
        // clearstatcache();
        // dd(filesize($upload_file));
        if($upload_file){
            $getFileExt   = $upload_file->getClientOriginalExtension();
            if($getFileExt == 'csv')
            {
                $data = $this->getCsvArray($upload_file);

              
                if(is_array($data))
                {
                    $mapping_array   =[
                        'no'=>'no',
                        'date'=>'date',
                        'academic_year'=>'academic_year',
                        'session'=>'session',
                        'alloted_category'=>'alloted_category',
                        'voucher_type'=>'voucher_type',
                        'voucher_no'=>'voucher_no',
                        'roll_no'=>'roll_no',
                        'admno_uniqueid'=>'admno_uniqueid',
                        'status'=>'status',
                        'fee_category'=>'fee_category',
                        'faculty'=>'faculty',
                        'program'=>'program',
                        'department'=>'department',
                        'batch'=>'batch',
                        'receipt_no'=>'receipt_no',
                        'fee_head'=>'fee_head',
                        'due_amount'=>'due_amount',
                        'paid_amount'=>'paid_amount',
                        'concession_amount'=>'concession_amount',
                        'scholarship_amount'=>'scholarship_amount',
                        'reverse_concession_amount'=>'reverse_concession_amount',
                        'write_off_amount'=>'write_off_amount',
                        'adjusted_amount'=>'adjusted_amount',
                        'refund_amount'=>'refund_amount',
                        'fund_trancfer_amount'=>'fund_trancfer_amount',
                        'remarks'=>'remarks'
                        ];
                    $keys   = array_keys($mapping_array);
                    $error  = [];
                    $loop   = 0;
                    $nostore= 0;
                    foreach($data as $each_data){
                        $tmp= [];
                        foreach($each_data as $k=>$v)
                        {
                            if(isset($mapping_array[$k])) $tmp[$mapping_array[$k]] = $v;
                        }
                        
            
                        if(count($tmp) > 0)
                        {
                           
                         // insert into branches table
                         $br_id = $this->branches($tmp['faculty']);
                         // insert feee category
                        $fee_category = $this->feecategory($br_id,$tmp['fee_category']);
                        // insert feecollectiontype
                         $Collection_id = $this->feecollectiontype($br_id);
                        // insert Fee_types
                        $this->Fee_types($br_id,$fee_category,$Collection_id,$tmp['fee_head'],$tmp['no']);
                        // insert financialtrans
                        $this->financialtrans($br_id,$tmp['date'],$tmp['voucher_no'],$tmp['paid_amount'],$tmp['academic_year']);
                        //Insert CommonFeeCollection        
                        $this->common_fee_collection($tmp['admno_uniqueid'], $tmp['roll_no'],$tmp['paid_amount'],$br_id,$tmp['academic_year'],$tmp['receipt_no'],$tmp['date']);  
                        //   
                        }
                        $loop++;
                    }
                  
                }
                else
                {
                    $return_message .= "File is not in proper format, Correct it and upload again !";
                }
            }
            else
            {
                $return_message .= "Please upload a CSV file";
            }
        }else
        {
            $return_message .= "Please select a CSV file to upload";
        }


      
        return redirect(route('import-files')); 

    }


    

    public function ImportCsvRules()
    {                       
        $rules = [
            'upload_file' => 'required|mimes:csv,txt|max:10000',
        ];
        return  $rules;
    }
    public function getCsvArray($csv_file)
    {
        $mandatory_fields   =['no','date','academic_year','session','alloted_category','voucher_type','voucher_no','roll_no','admno_uniqueid','status','fee_category','faculty','program','department','batch','receipt_no','fee_head','due_amount','paid_amount','concession_amount','scholarship_amount','reverse_concession_amount','write_off_amount','adjusted_amount','refund_amount','fund_trancfer_amount','remarks'];

     
        $store_details      = [];
        $file_elements      = fopen($csv_file,"r");
        $field_names        = fgetcsv($file_elements);
        foreach($field_names as $each_field){
            if(!empty($each_field)){
                $v1=strtolower($each_field);
                $v1=str_replace('  ',' ', $v1);
                $v1=str_replace(' ','_', $v1);
                $v1=str_replace('/','_', $v1);
                $v1=str_replace('.','', $v1);
                $v1=str_replace('﻿','', $v1);
                $field_names_new[]=trim($v1);
            }
        }
        foreach($mandatory_fields as $each_mandatory_field){
            if(in_Array($each_mandatory_field,$field_names_new)){
                $flag   = 'valid';
                continue;
            }else{
                $flag   = 'invalid';
                break;
            }
        }
        if($flag == 'invalid')    	
        {
            return "Sheet is invalid";
        }
        while(($arr = fgetcsv($file_elements))){
            $tmp = [];
            foreach($field_names_new as $k => $v) {
                if(!empty($v)){
                    $v1=strtolower($v);
                    $v1=str_replace('  ',' ', $v1);
                    $v1=str_replace(' ','_', $v1);
                    $v1=str_replace('/','_', $v1);
                    $v1=str_replace('.','', $v1);
                    if(isset($arr[$k])) {
                        $tempVal=str_replace('  ',' ', $arr[$k]);
                        $tempVal=str_replace(' \' ',' ', $arr[$k]);
                        $tempVal=str_replace("\'",'', $arr[$k]);
                        $tempVal=str_replace('\ ','', $arr[$k]);
                        $tempVal=str_replace("'",'', $arr[$k]);
                        $tempVal=str_replace('"','', $arr[$k]);
    
                        $tmp[$v1] = $tempVal;
                    } else {
                        $tmp[$v] = '';
                    }    
                }
            
            }
            $store_details[] = $tmp;
        }
        return $store_details;
    }
    public function branches($branch) {

        $data = new branches();
        $data->Branch_name = $branch;
        $data->save();
        return $data->id;

    }
    public function feecategory($br_id,$fee_category) {


        $data = new feecategory();
        $data->fee_category = $fee_category;
        $data->br_id = $br_id;
        $data->save();
        return $data->id;

    }
    public function feecollectiontype($br_id) {
        $data = new feecollectiontype();
        $data->collectionhead = "Academic";
        $data->collectiondesc = "Academic";    
        $data->br_id = $br_id;
        $data->save();
        return $data->id;
    }
    

    public function Fee_types($br_id,$fee_category,$Collection_id,$fee_head,$Seq_id) {
        $data = new Fee_types();
        $data->fee_category = $fee_category;
        $data->f_name = $fee_head;    
        $data->Collection_id = $Collection_id;
        $data->br_id = $br_id;
        $data->Seq_id = $Seq_id;
        $data->Fee_type_ledger = $fee_head;
        $data->Fee_headtype = 1;
        $data->save();
        return $data->id;
    }

    public function financialtrans($br_id,$date,$voucher_no,$paid_amount,$academic_year) {
        $data = new Financial_trans();
        $data ->moduleid ='1';
        $data ->tranid = '396944';
        $data ->amount = $paid_amount;
        $data ->crdr = "Dr";
        $data ->trandate = $date;
        $data ->acadyear = $academic_year;
        $data ->entrymode = "0";
        $data ->voucherno = $voucher_no;
        $data ->brid = $br_id;
        $data->save();
        return $data->id;

    }

    public function financial_transdetails($br_id,$paid_amount,$financial_tranId,$fee_head){
        $data = new financial_transdetails();
        $data ->financialTranId = $financial_tranId;     
        $data ->moduleId = '1';
        $data ->amount = $paid_amount;
        $data ->headId = '495609';
        $data ->crdr = 'Dr';
        $data ->brid = $br_id;
        $data ->head_name = $fee_head;
        $data->save();
        return $data->id;
    }

    public function common_fee_collection($admno_uniqueid, $roll_no,$paid_amount,$br_id,$academic_year,$receipt_no,$date){
        $data = new Common_fee_collection();
        $data ->moduleId = '1';
        $data ->transId = '396944';
        $data ->admno = $admno_uniqueid;
        $data ->rollno = $roll_no;
        $data ->amount = $paid_amount;
        $data ->brId = $br_id;
        $data ->academicYear = $academic_year;
        $data ->financialYear = $academic_year;
        $data ->displayReceiptNo = $receipt_no;
        $data ->Entrymode = '0';
        $data ->Paid_Date = $date;
        $data->save();
        return $data->id;
    }

    public function common_fee_collection_headwise($receipt_no,$fee_head,$paid_amount,$br_id){
        $data = new common_fee_collection_headwise();
        $data ->moduleId = '1';
        $data ->receiptId = $receipt_id ;
        $data ->headId = '495609';
        $data ->headName = $fee_head;
        $data ->brid = $br_id;
        $data ->amount = $paid_amount;
        $data -> save();
        return $data->id;
    }
    
}
