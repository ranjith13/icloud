@extends('admin.layouts.app')
@section('title', 'Masters')
@section('subtitle', 'Masters')
@section('content')
<div class="row">
<!-- Branches starts here -->
<div class="col-md-3 col-sm-6 col-12">
<a href="/admin/branch">

            <div class="info-box">
              <span class="info-box-icon bg-info"><i class="far fa-envelope"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Branches</span>
               <!--  <span class="info-box-number">1,410</span> -->
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
            </a>
</div>
<!-- Branches ends here -->

<!-- feecategory starts here -->
<div class="col-md-3 col-sm-6 col-12">
<a href="/admin/fee-category">
            <div class="info-box">
              <span class="info-box-icon bg-info"><i class="far fa-envelope"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Fee Category</span>
               <!--  <span class="info-box-number">1,410</span> -->
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
            </a>
</div>
<!-- feecategory ends here -->


<!-- feecollectiontype starts here -->
<div class="col-md-3 col-sm-6 col-12">
<a href="/admin/fee-coll-type">
            <div class="info-box">
              <span class="info-box-icon bg-info"><i class="far fa-envelope"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Fee Collection type</span>
               <!--  <span class="info-box-number">1,410</span> -->
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
            </a>
</div>
<!-- feecollectiontype ends here -->


<!-- Fee_types starts here -->
<div class="col-md-3 col-sm-6 col-12">
<a href="/admin/fee-types">

            <div class="info-box">
              <span class="info-box-icon bg-info"><i class="far fa-envelope"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Fee Types</span>
               <!--  <span class="info-box-number">1,410</span> -->
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
            </a>
</div>
<!-- Fee_types ends here -->


<!-- entrymode starts here -->
<div class="col-md-3 col-sm-6 col-12">
            <div class="info-box">
              <span class="info-box-icon bg-info"><i class="far fa-envelope"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Entry Mode</span>
               <!--  <span class="info-box-number">1,410</span> -->
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
</div>
<!-- entrymode ends here -->

<!-- Financial_trans starts here -->
<div class="col-md-3 col-sm-6 col-12">
            <div class="info-box">
              <span class="info-box-icon bg-info"><i class="far fa-envelope"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Financial Trans</span>
               <!--  <span class="info-box-number">1,410</span> -->
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
</div>
<!-- Financial_trans ends here -->



<!-- Financial_transdetail starts here -->
<div class="col-md-3 col-sm-6 col-12">
            <div class="info-box">
              <span class="info-box-icon bg-info"><i class="far fa-envelope"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Financial Trans Details</span>
               <!--  <span class="info-box-number">1,410</span> -->
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
</div>
<!-- Financial_transdetail ends here -->


<!-- Import starts here -->
<div class="col-md-3 col-sm-6 col-12">
<a href="/admin/import-files">
            <div class="info-box">
              <span class="info-box-icon bg-info"><i class="far fa-envelope"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Import</span>
               <!--  <span class="info-box-number">1,410</span> -->
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
   </a>         
</div>
<!-- Import ends here -->






</div>
@endsection
