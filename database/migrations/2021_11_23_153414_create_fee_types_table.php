<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFeeTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fee_types', function (Blueprint $table) {
            $table->id();
            $table->integer('fee_category');
            $table->string('f_name', 100);
            $table->integer('Collection_id');
            $table->integer('br_id'); 
            $table->integer('Seq_id'); 
            $table->string('Fee_type_ledger', 100); 
            $table->integer('Fee_headtype');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fee_types');
    }
}
