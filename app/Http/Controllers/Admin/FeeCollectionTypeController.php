<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Announcement;
use App\Http\Requests;
use App\Models\branches;
use App\Models\feecategory;
use App\Models\feecollectiontype;
use App\Models\Fee_types;
use DataTables;
use DB;

class FeeCollectionTypeController extends Controller
{
    public function index(Request $request) {
        if ($request->ajax()) { 
            $data   = feecollectiontype::select('*');

            return Datatables::of($data)
            ->addIndexColumn()
            ->make(true);
        }
       return view("admin.feecollection.feecoll");
    }
}