<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/home', [App\Http\Controllers\Admin\AdminController::class, 'index']);
Route::get('/masters', [App\Http\Controllers\Admin\AdminController::class, 'masters']);
Route::get('/import-files', [App\Http\Controllers\Admin\AdminController::class, 'importview'])->name('import-files');;
Route::post('/file-import', [App\Http\Controllers\Admin\AdminController::class, 'import'])->name('file-import');


Route::get('/branch', [App\Http\Controllers\Admin\BranchController::class, 'index'])->name('branch');
Route::get('/fee-category', [App\Http\Controllers\Admin\FeeCatController::class, 'index'])->name('fee-category');
Route::get('/fee-coll-type', [App\Http\Controllers\Admin\FeeCollectionTypeController::class, 'index'])->name('fee-coll-type');
Route::get('/fee-types', [App\Http\Controllers\Admin\FeeTypeController::class, 'index'])->name('fee-types');




