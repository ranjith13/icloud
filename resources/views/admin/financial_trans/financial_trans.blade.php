@extends('admin.layouts.app')
@section('title', 'Financial_trans')
@section('subtitle', 'Financial_trans')
@section('content')
<div class="card-body">
<table class="table table-bordered table-striped table-financial_trans" style="width:100%">
<thead>
<tr>
<th>S. No.</th>
<th>ModuleId</th> 
<th>Transaction Id</th>
<th>Amount</th>
<th>Cr/Dr</th>
<th>Transaction Date</th>
<th>Academic Year</th>
<th>Entry Mode</th>
<th>Voucher No</th>
<th>Branch Id</th>
<th>Due Amount</th> 
<th>Consession/Scholarship</th>  
<th>Due Rev</th>  
<th>Consession/Scholarship Rev</th>                                     
</tr>
</thead>
</table>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
  $(function () {
    $(".loader").hide();
    $(".data-section").show();
    var table = $('.table-financial_trans').DataTable({
        bStateSave: true,
        responsive: true,
        bDestroy: true,
        processing: true,
        serverSide: true,
        deferRender: true,
        "pageLength": 10,
        "scrollX": true,
        dom: 'Bfrtip',
        lengthMenu: [
            [ 10, 25, 50, 500 ],
            [ '10 rows', '25 rows', '50 rows', '500 rows' ]
        ],
        buttons: [
            'pageLength','colvis','copy', 'csv', 'excel', 'print'
        ],
        ajax: "{{ route('branch') }}",
        columns: [
            {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: false, searchable: false},
            {data: 'moduleid', name: 'moduleid'}
            {data: 'tranid', name: 'tranid'}
            {data: 'amount', name: 'amount'}
            {data: 'crdr', name: 'crdr'}
            {data: 'tranDate', name: 'tranDate'}
            {data: 'acadYear', name: 'acadYear'}
            {data: 'entrymode', name: 'entrymode'}
            {data: 'Voucherno', name: 'Voucherno'}
            {data: 'brid', name: 'brid'}
            {data: 'dueAmount', name: 'dueAmount'}
            {data: 'consession/scholarship', name: 'consession/scholarship'}
            {data: 'duerev', name: 'duerev'}
            {data: 'consession/scholarshipRev', name: 'consession/scholarshipRev'}
             ]
    });
  });
</script>
@endsection

